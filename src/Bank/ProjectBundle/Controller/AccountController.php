<?php

namespace Bank\ProjectBundle\Controller;

use Symfony\Component\HttpFoundation\Response;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

use Bank\ProjectBundle\Entity\Account;
use Bank\ProjectBundle\Form\AccountType;

/**
 * Account controller.
 *
 */
class AccountController extends Controller
{

    /**
     * Lists all Account entities.
     *
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();

        $user = $this->container->get('security.context')->getToken()->getUser();

        $entities = $em->getRepository('BankProjectBundle:Account')->findByOwner($user->getId());

        return $this->render('BankProjectBundle:Account:index.html.twig', array(
            'entities' => $entities,
        ));
    }

    /**
     * Finds and displays a Account entity.
     *
     */
    public function showAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('BankProjectBundle:Account')->find($id);
        $user = $this->container->get('security.context')->getToken()->getUser();

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Account entity.');
        }

        if($entity->getOwner()->getId() != $user->getId())
            throw $this->createAccessDeniedException("You cannot view this account");

        $transactions = $em->getRepository('BankProjectBundle:Transaction')->getTransactionsFromAccount($entity)->getResult();

        return $this->render('BankProjectBundle:Account:show.html.twig', array(
            'entity'      => $entity,
            'transactions' => $transactions,
        ));
    }

    public function showBankStatementAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('BankProjectBundle:Account')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Account entity.');
        }

        $transactions = $this->getTransactionOfPreviousMonth($entity);

        return $this->render('BankProjectBundle:Account:show.html.twig', array(
            'entity'      => $entity,
            'transactions' => $transactions,

        ));
    }

    public function showBankStatementExportableAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('BankProjectBundle:Account')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Account entity.');
        }

        $transactions = $this->getTransactionOfPreviousMonth($entity);

        return $this->render('BankProjectBundle:Account:showExportable.html.twig', array(
            'entity'      => $entity,
            'transactions' => $transactions,
        ));
    }

    public function showBankStatementPDFAction($id)
    {
        $pageUrl = $this->generateUrl('account_statement_exportable_show', array('id' => $id), true);

        $filename = "Statement-".$id."-".$this->toStringPreviousMonth().".pdf";

        return new Response(
            $this->get('knp_snappy.pdf')->getOutput($pageUrl),
            200,
            array(
                'Content-Type'          => 'application/pdf',
                'Content-Disposition'   => "attachment; filename=$filename"
            )
        );
    }

    public function getTransactionOfPreviousMonth($account)
    {
        $em = $this->getDoctrine()->getManager();

        $currentDate = new \DateTime();
        $currentYear = intval($currentDate->format("Y"));
        $currentMonth = intval($currentDate->format("m"));

        $begin = new \DateTime();;
        $begin->setDate($currentYear, $currentMonth - 1, 1)
            ->setTime(0,0,1);
        $end = new \DateTime();
        $end->setDate($currentYear, $currentMonth, 1)
            ->setTime(0,0,0);

        return $em->getRepository('BankProjectBundle:Transaction')->getTransactionsFromAccountAndInterval($account, $begin, $end)->getResult();
    }

    public function toStringPreviousMonth()
    {
        $currentDate = new \DateTime();
        return $currentDate->format("M").$currentDate->format("Y");
    }
}

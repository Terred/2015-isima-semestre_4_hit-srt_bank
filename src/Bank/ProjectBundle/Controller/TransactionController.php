<?php

namespace Bank\ProjectBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

use Bank\ProjectBundle\Entity\Transaction;
use Bank\ProjectBundle\Form\TransactionType;
use Symfony\Component\Validator\Constraints\DateTime;

/**
 * Transaction controller.
 *
 */
class TransactionController extends Controller
{
    /**
     * Creates a new Transaction entity.
     *
     */
    public function createAction(Request $request)
    {
        $entity = new Transaction();
        $form = $this->createCreateForm($entity);
        $form->handleRequest($request);

        if ($form->isValid()) {

            $entity->setType("Transfert - WebSite");
            $entity->setDate(new \DateTime());

            $em = $this->getDoctrine()->getManager();

            $source = $em->getRepository('BankProjectBundle:Account')->find($entity->getSource());
            $source->setCurrentMoney($source->getCurrentMoney() - $entity->getAmount());
            $destination = $em->getRepository('BankProjectBundle:Account')->find($entity->getDestination());
            $destination->setCurrentMoney($destination->getCurrentMoney() + $entity->getAmount());

            $em->persist($entity);
            $em->flush();

            return $this->redirect($this->generateUrl('bank_project_homepage'));
        }

        return $this->render('BankProjectBundle:Transaction:new.html.twig', array(
            'entity' => $entity,
            'form'   => $form->createView(),
        ));
    }

    /**
     * Creates a form to create a Transaction entity.
     *
     * @param Transaction $entity The entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createCreateForm(Transaction $entity)
    {
        $user = $this->container->get('security.context')->getToken()->getUser();
        $accounts = $user->getAccounts();
        foreach($accounts as $account)
        {
            $arrayAccounts[$account->getId()] = $account->getAccountLabel();
        }

        $form = $this->createForm(new TransactionType(), $entity, array(
            'action' => $this->generateUrl('transaction_create'),
            'method' => 'POST',
            'accounts' => $arrayAccounts,
        ));

        $form->add('submit', 'submit', array('label' => 'Create'));

        return $form;
    }

    /**
     * Displays a form to create a new Transaction entity.
     *
     */
    public function newAction()
    {
        $entity = new Transaction();
        $form   = $this->createCreateForm($entity);

        return $this->render('BankProjectBundle:Transaction:new.html.twig', array(
            'entity' => $entity,
            'form'   => $form->createView(),
        ));
    }
}

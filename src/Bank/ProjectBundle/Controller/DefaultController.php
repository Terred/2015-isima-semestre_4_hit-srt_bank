<?php

namespace Bank\ProjectBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class DefaultController extends Controller
{
    public function indexAction()
    {
        return $this->render('BankProjectBundle:Default:index.html.twig');
    }

    public function contactUsAction()
    {
        return $this->render('BankProjectBundle:Default:contactus.html.twig');
    }
}
